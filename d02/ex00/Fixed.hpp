/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:18:57 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 08:23:11 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FIXED_HPP
# define FIXED_HPP

class	Fixed 
{
	private:

		int					_raw;
		static int const	_bit = 8;
	public:

		Fixed(void);
		Fixed(Fixed const & s);
		~Fixed(void);

		Fixed &		operator=(Fixed const & n);

		int		getRawBits(void) const;
		void	setRawBits(int const raw);

};

#endif
