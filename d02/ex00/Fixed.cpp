/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:18:57 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 11:52:18 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed(void): _raw(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(Fixed const & c)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = c;
}

Fixed::~Fixed(void) {

	std::cout << "Destructor called" << std::endl;
}

int		Fixed::getRawBits(void) const {

	std::cout << "GetRawBits member function called" << std::endl;
	return this->_raw;
}

void    Fixed::setRawBits(int const raw) {

	this->_raw = raw;
}

Fixed &		Fixed::operator=(Fixed const & n) {

	std::cout << "Assignation operator called" << std::endl;
	if (this != &n)
		this->_raw = n.getRawBits();
	return *this;
}
