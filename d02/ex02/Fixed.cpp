#include "Fixed.hpp"
#include <iostream>
#include <cmath>

Fixed::Fixed(void): _raw(0) {}

Fixed::Fixed(int const n)
{
	setRawBits(n << _bit);
}

Fixed::Fixed(float const n)
{
	setRawBits(roundf(n * (1 << _bit)));
}

Fixed::Fixed(Fixed const & c)
{
	*this = c;
}

Fixed::~Fixed(void) {}

int		Fixed::getRawBits(void) const
{
	return this->_raw;
}

void    Fixed::setRawBits(int const raw)
{
	this->_raw = raw;
}

int		Fixed::toInt(void) const
{
	return (getRawBits() >> this->_bit);
}

float	Fixed::toFloat(void) const
{
	float	n;

	n = getRawBits();
	return (n / (1 << this->_bit));
}

Fixed &		Fixed::operator=(Fixed const & n)
{
	if (this != &n)
		this->_raw = n.getRawBits();

	return *this;
}

bool	Fixed::operator>(Fixed const & n)
{
	return (this->_raw > n.getRawBits());
}

bool	Fixed::operator<(Fixed const & n)
{
    return (this->_raw < n.getRawBits());
}

bool	Fixed::operator>=(Fixed const & n)
{
    return (this->_raw >= n.getRawBits());
}

bool	Fixed::operator<=(Fixed const & n)
{
    return (this->_raw <= n.getRawBits());
}

bool	Fixed::operator==(Fixed const & n)
{
    return (this->_raw == n.getRawBits());
}

bool	Fixed::operator!=(Fixed const & n)
{
    return (this->_raw != n.getRawBits());
}

Fixed	Fixed::operator+(Fixed const & n)
{
	Fixed	tmp;

	tmp.setRawBits(this->_raw + n._raw);
	return (tmp);
}

Fixed 	Fixed::operator-(Fixed const & n)
{
    Fixed   tmp;

    tmp.setRawBits(this->_raw - n._raw);
    return (tmp);
}

Fixed 	Fixed::operator*(Fixed const & n)
{
    Fixed   tmp;

    tmp._raw = (this->_raw * n._raw) >> this->_bit;
    return (tmp);
}

Fixed 	Fixed::operator/(Fixed const & n)
{
    Fixed   tmp;

    tmp._raw = (this->_raw << this->_bit) / n._raw;
    return (tmp);
}

Fixed &		Fixed::operator++(void)
{
	this->_raw++;
	return (*this);
}

Fixed		Fixed::operator++(int)
{
	Fixed	newfix;

	newfix = *this;
	this->_raw++;
	return (newfix);
}

Fixed &		Fixed::operator--(void)
{
	this->_raw--;
	return (*this);
}

Fixed		Fixed::operator--(int)
{
	Fixed	newfix;

	newfix = *this;
	this->_raw++;
	return (newfix);
}

Fixed & 			Fixed::min(Fixed & t, Fixed & n)
{
	if (t.getRawBits() < n.getRawBits())
		return (t);
	return (n);
}

Fixed & 			Fixed::max(Fixed & t, Fixed & n)
{
	if (t.getRawBits() > n.getRawBits())
		return (t);
	return (n);
}

const Fixed & 	Fixed::min(Fixed const & t, Fixed const & n)
{
	if (t.getRawBits() < n.getRawBits())
		return (t);
	return (n);
}

const Fixed & 	Fixed::max(Fixed const & t, Fixed const & n)
{
	if (t.getRawBits() > n.getRawBits())
		return (t);
	return (n);
}

std::ostream &	operator<<(std::ostream & o, Fixed const & n)
{
	o << n.toFloat();
	return o;
}
