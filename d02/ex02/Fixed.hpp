#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>

class	Fixed 
{

	private:

		int					_raw;
		static int const	_bit = 8;

	public:

		Fixed(void);
		Fixed(int const n);
		Fixed(float const n);
		Fixed(Fixed const & c);
		~Fixed(void);

		Fixed &		operator=(Fixed const & n);

		bool		operator>(Fixed const & n);
		bool		operator<(Fixed const & n);
		bool		operator>=(Fixed const & n);
		bool		operator<=(Fixed const & n);
		bool		operator==(Fixed const & n);
		bool		operator!=(Fixed const & n);

		Fixed 		operator+(Fixed const & n);
		Fixed 		operator-(Fixed const & n);
		Fixed 		operator*(Fixed const & n);
		Fixed 		operator/(Fixed const & n);

		Fixed &		operator++(void);
		Fixed		operator++(int);
		Fixed &		operator--(void);
		Fixed		operator--(int);

		int			getRawBits(void) const;
		void		setRawBits(int const raw);
		float		toFloat(void) const;
		int			toInt(void) const;

		static Fixed & 			min(Fixed & t, Fixed & n);
		static Fixed & 			max(Fixed & t, Fixed & n);
		static const Fixed & 	min(Fixed const & t, Fixed const & n);
		static const Fixed & 	max(Fixed const & t, Fixed const & n);

};

std::ostream &	operator<<(std::ostream & o, Fixed const & n);

#endif
