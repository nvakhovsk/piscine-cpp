/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:18:57 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 08:42:11 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.hpp"

int 	main( void )
{
    Fixed a;
    Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );

	std::cout << a << std::endl;
	std::cout << ++a << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << Fixed::max( a, b ) << std::endl;


	Fixed c(3);
	Fixed d(7);

	std::cout << "c = " << c << std::endl;
	std::cout << "d = " << d << std::endl;
	if (c > d)
		std::cout << "c > d" << std::endl;
	if (c < d)
		std::cout << "c < d" << std::endl;
	if (c >= d)
		std::cout << "c >= d" << std::endl;
	if (d <= c)
		std::cout << "d <= c" << std::endl;
	if (d == c)
		std::cout << "d == c" << std::endl;
	if (d != c)
		std::cout << "d != c" << std::endl;
	std::cout << "c + d = " << (c + d) << std::endl;
	std::cout << "c - d = " << (c - d) << std::endl;
	std::cout << "c * d = " << (c * d) << std::endl;
	std::cout << "c / d = " << (c / d) << std::endl;	
	return (0);
}
