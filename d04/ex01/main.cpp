/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:11:41 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Character.hpp"
#include "RadScorpion.hpp"
#include "Enemy.hpp"
#include "PowerFist.hpp"
#include "PlasmaRifle.hpp"
#include "SuperMutant.hpp"

int main()
{
	Character* zaz = new Character("zaz");

	std::cout << *zaz;

	Enemy* b = new RadScorpion();
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();

	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);

	zaz->attack(b);
	std::cout << *zaz;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;

	Enemy* t = new SuperMutant();
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << "the Super Mutant has "<< t->getHP() << "HP!"<< std::endl;
	std::cout << *zaz;
	zaz->recoverAP();
	std::cout << *zaz;
	zaz->recoverAP();
	std::cout << *zaz;
	zaz->recoverAP();
	std::cout << *zaz;
	zaz->recoverAP();
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << "the Super Mutant has "<< t->getHP() << "HP!"<< std::endl;
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << "the Super Mutant has "<< t->getHP() << "HP!"<< std::endl;
	std::cout << *zaz;
	zaz->attack(t);
	std::cout << *zaz;

	return 0;
}
