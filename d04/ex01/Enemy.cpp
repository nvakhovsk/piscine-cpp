/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:55 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.hpp"

Enemy::Enemy() : _type(""), _hp(0){}

Enemy::Enemy(Enemy const & n) : _type(n._type), _hp(n._hp) {}

Enemy::Enemy(int hp, std::string const & type) : _type(type), _hp(hp) {}

Enemy::~Enemy(){}

std::string  const &	Enemy::getType() const
{
	return _type;
}

int 				Enemy::getHP() const
{
	return _hp;
}

void 		Enemy::takeDamage(int damage)
{
	if (_hp > 0 && damage > 0)
	{
		if (_hp - damage > 0)
			_hp -= damage;
		else
			_hp = 0;
	}
}

Enemy & 			Enemy::operator=(Enemy const & r)
{
	if (this != &r)
		_hp = r._hp;
	return *this;
}