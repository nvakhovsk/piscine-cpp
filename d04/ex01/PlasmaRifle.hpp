/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:11:52 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "AWeapon.hpp"

#ifndef PLASMARIFLE_HPP
 #define PLASMARIFLE_HPP
class PlasmaRifle : public AWeapon
{
	public:
		PlasmaRifle(PlasmaRifle const & n);
		PlasmaRifle();
		~PlasmaRifle();
		using AWeapon::operator=;

		void			attack() const;
};
#endif
