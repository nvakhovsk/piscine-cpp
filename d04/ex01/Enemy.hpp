/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:59 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENNEMY_HPP
 #define ENNEMY_HPP

#include <string>

class Enemy
{
	protected:
		std::string const	_type;
		int					_hp;

	public:
		Enemy();
		Enemy(Enemy const &);
		Enemy(int hp, std::string const & type);
		virtual ~Enemy();

		std::string const &		getType() const;
		int 					getHP() const;

		virtual void 			takeDamage(int);

		Enemy &					operator=(Enemy const & r);
};

#endif
