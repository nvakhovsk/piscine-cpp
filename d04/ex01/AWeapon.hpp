/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:15:15 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

#ifndef AWEAPON_HPP
 #define AWEAPON_HPP
class AWeapon
{
	protected:
		std::string			_name;
		int					_apcost;
		int 				_damage;
		
	public:
		AWeapon();
		AWeapon(AWeapon const & n);
		AWeapon(std::string const & name, int apcost, int damage);
		virtual ~AWeapon();

		std::string const &		getName() const;
		int 					getAPCost() const;
		int 					getDamage() const;
		virtual void			attack() const = 0;

		AWeapon &			operator=(AWeapon const & r);
};
#endif
