/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:50 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_HPP
 #define CHARACTER_HPP

#include <string>
#include <iostream>
#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character
{
	private:
		std::string const		_name;
		AWeapon	*				_weapon;
		int						_ap;

	public:
	Character(std::string const & name);
	Character();
	Character(Character const &n);
	~Character();

	std::string const &		getName() const;
	int 					getAP()const;
	AWeapon *				getWeapon()const;

	void					recoverAP();
	void					equip(AWeapon*);
	void					attack(Enemy*);
	
	Character & operator=(Character const &r);
};

std::ostream & operator<<(std::ostream & o, Character const & r);
#endif