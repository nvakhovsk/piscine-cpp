/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:11:58 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "AWeapon.hpp"

#ifndef POWERFIST_HPP
 #define POWERFIST_HPP
class PowerFist  : public AWeapon
{
	public:
		PowerFist(PowerFist const & n);
		PowerFist();
		~PowerFist();
		using AWeapon::operator=;

		void			attack() const;
};
#endif
