/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:47 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Character.hpp"
#include <string>

class Enemy;
class AWeapon;

Character::Character(std::string const & name) :
		_name(name), _weapon(NULL), _ap(40) {}

Character::Character() : _name(""), _weapon(NULL), _ap(40) {}

Character::Character(Character const & n) :
	_name(n._name), _weapon(n._weapon), _ap(n._ap) {}

Character::~Character(){}

std::string const &	Character::getName() const
{
	return _name;
}

int  			Character::getAP() const
{
	return _ap;
}

AWeapon *		Character::getWeapon() const
{
	return _weapon;
}

void Character::recoverAP()
{
	if (_ap + 10 < 40)
		_ap += 10;
	else
		_ap = 40;
}

void Character::equip(AWeapon* weapon)
{
	_weapon =  weapon;
}

void Character::attack(Enemy* enemy)
{
	if (_ap >= _weapon->getAPCost())
	{
		_ap -= _weapon->getAPCost();
		enemy->takeDamage(_weapon->getDamage());
		std::cout << _name <<" attacks "<< enemy->getType() <<" with a "<< _weapon->getName() << std::endl;
		if (enemy->getHP() <= 0)
			delete enemy;
	}
}

Character & Character::operator=(Character const & r)
{
	if (this != &r)
	{
		_ap = r._ap;
		_weapon = r._weapon;
	}
	return *this;
}

std::ostream & operator<<(std::ostream & o, Character const & r) {
	if (r.getWeapon())
		o << r.getName()<< " has "<< r.getAP() <<" AP and wields a " << r.getWeapon()->getName() << std::endl;
	else
		o << r.getName()<< " has "<< r.getAP() <<" AP and is unarmed" << std::endl;
	return o;
}
