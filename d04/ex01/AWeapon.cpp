/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:38 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "AWeapon.hpp"

AWeapon::AWeapon() : _name(""), _apcost(0), _damage(0) {}

AWeapon::AWeapon(std::string const & name, int apcost, int damage) :
							_name(name), _apcost(apcost), _damage(damage) {}

AWeapon::AWeapon(AWeapon const & n) :
			_name(n._name), _apcost(n._apcost), _damage(n._damage) {}

AWeapon::~AWeapon() {}

std::string const & AWeapon::getName() const
{
	return _name;
}

int AWeapon::getAPCost() const
{
	return _apcost;
}

int AWeapon::getDamage() const
{
	return _damage;
}

AWeapon &		AWeapon::operator=(AWeapon const & r)
{
	if (this != &r)
	{
		_name = r._name;
		_apcost = r._apcost;
		_damage = r._damage;
	}
	return *this;
}
