/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:20 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.hpp"
#include <iostream>

Sorcerer::Sorcerer(std::string name, std::string title)
{
	_name = name;
	_title = title;
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;	
}

Sorcerer::Sorcerer(Sorcerer & n)
{
	*this = n;
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;	
}

Sorcerer::~Sorcerer()
{
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;	
}

std::string	Sorcerer::getName() const
{
	return (this->_name);
}

std::string	Sorcerer::getTitle() const
{
	return (this->_title);
}

Sorcerer &	Sorcerer::operator=(Sorcerer & r)
{
	this->_name = r._name;
	this->_title = r._title;
	return *this;
}

std::ostream &	operator<<(std::ostream & o, Sorcerer const & r)
{
	o << "I am " << r.getName() << ", " << r.getTitle() << ", and I like ponies !" << std::endl;	
	return o;
}

void	Sorcerer::polymorph(Victim const & src) const
{
	src.getPolymorphed();
}