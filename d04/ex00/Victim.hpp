/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:31 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP

#include <iostream>

class Victim
{
	protected:
		std::string	_name;
	public:
		Victim();
		Victim(std::string name);
		Victim(Victim & n);
		virtual ~Victim();

		std::string		getName() const;
		virtual void	getPolymorphed() const;

		Victim &	operator=(Victim & r);
};

std::ostream &	operator<<(std::ostream & o, Victim const & r);

#endif