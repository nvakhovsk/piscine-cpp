/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:10:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP

#include "Victim.hpp"
#include <iostream>

class Sorcerer
{
	private:
		std::string	_name;
		std::string	_title;
	public:
		Sorcerer();
		Sorcerer(std::string name, std::string title);
		Sorcerer(Sorcerer & n);
		~Sorcerer();
	
		std::string	getName() const;
		std::string	getTitle() const;

		Sorcerer &	operator=(Sorcerer & r);

		void		polymorph(Victim const &) const;
};

std::ostream &	operator<<(std::ostream & o, Sorcerer const & r);

#endif