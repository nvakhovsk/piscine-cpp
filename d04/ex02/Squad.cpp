/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:12:51 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.hpp"
#include <iostream>

Squad::Squad()
{
	_container = NULL;
	_nbr = 0;
}

Squad::Squad( Squad const & n )
{
	t_list	*t;
	t_list	*nt;

	t = _container;
	nt = n._container;
	_container = NULL;
	while (nt)
	{
		if (_container != NULL)
		{
			t = _container;
			while (t->next)
				t = t->next;
			t->next = new(t_list);
			t->next->current = nt->current;
			t->next->next = NULL;
		}
		else
		{
			_container = new(t_list);
			_container->current = nt->current;
			_container->next = NULL;
		}
		nt = nt->next;
	}
	this->_nbr = n._nbr;
}

Squad::~Squad()
{
	t_list	*t;

	while (_container)
	{
		t = _container;
		delete _container->current;
		_container = _container->next;
		delete t;
	}
}

int		Squad::getCount() const
{
	return _nbr;
}

ISpaceMarine*	Squad::getUnit(int i) const
{
	t_list	*t;

	t = _container;
	while (i != 0 && t)
	{
		t = t->next;
		i--;
	}
	return (t->current);
}

int		Squad::push(ISpaceMarine* n)
{
	t_list	*t;

	if (n != NULL)
	{
		if (_container != NULL)
		{
			t = _container;
			while (t->next)
			{
				if (t->current == n)
					return (_nbr);
				t = t->next;
			}
			t->next = new(t_list);
			t->next->current = n;
			t->next->next = NULL;
		}
		else
		{
			_container = new(t_list);
			_container->current = n;
			_container->next = NULL;
		}
		_nbr++;
	}
	return (_nbr);
}

Squad&  Squad::operator=(Squad const & r)
{
    t_list  *t;
    t_list	*nt;

    t= _container;
    while (t)
    {
		delete t->current;
    }
    if (this != &r)
		_nbr = r._nbr;

	t = _container;
	nt = r._container;
	_container = NULL;

	while (nt)
	{
		if (_container != NULL)
		{
			t = _container;
			while (t->next)
				t = t->next;
			t->next = new(t_list);
			t->next->current = nt->current;
			t->next->next = NULL;
		}
		else
		{
			_container = new(t_list);
			_container->current = nt->current;
			_container->next = NULL;
		}
		nt = nt->next;
	}
    return *this;
}
