/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:12:54 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SQUAD_HPP
# define SQUAD_HPP

#include "ISquad.hpp"

class Squad : public ISquad
{
	typedef struct s_list
	{
		ISpaceMarine	*current;
		s_list			*next;
	}				t_list;

	private:
		int		_nbr;
		t_list	*_container;

	public:

		Squad();
		Squad(Squad const & n);
		~Squad();

		int				getCount() const;
		ISpaceMarine*	getUnit( int i ) const;

		int		push(ISpaceMarine* n);
		
		Squad& operator=(Squad const & r);
};

#endif
