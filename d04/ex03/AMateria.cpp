/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:13:15 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.hpp"
#include <iostream>

AMateria::AMateria(void): type_(""), xp_(0) {}

AMateria::AMateria(std::string const & type): type_(type), xp_(0) {}

AMateria::AMateria(AMateria const & n)
{
	*this = n;
}

AMateria::~AMateria() {}

std::string const &	AMateria::getType() const
{
	return (type_);
}

unsigned int		AMateria::getXP() const
{
	return (xp_);
}

void				AMateria::setXP()
{
	xp_ += 10;
}

void		AMateria::use(ICharacter& target)
{
	(void)target;
}

AMateria &	AMateria::operator=(AMateria const & r)
{
	xp_ = r.getXP();
	return (*this);
}