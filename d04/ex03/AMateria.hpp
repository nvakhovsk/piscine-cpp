/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:13:19 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AMATERIA_HPP
# define AMATERIA_HPP

#include "ICharacter.hpp"
#include <iostream>

class AMateria
{
	private:

		std::string		type_;
		unsigned int 	xp_;

	public:

		AMateria();
		AMateria(std::string const & type);
		AMateria(AMateria const & n);
		virtual ~AMateria();

		std::string const &	getType() const;
		unsigned int		getXP() const;
		void				setXP();
		
		virtual AMateria*	clone() const = 0;
		virtual void		use(ICharacter& target);

		AMateria &	operator=(AMateria const & r);
};

#endif