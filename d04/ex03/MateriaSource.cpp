/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:14:15 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.hpp"
#include "AMateria.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include <iostream>

MateriaSource::MateriaSource()
{
	_nmap = new AMateria*[4];
	for (int i = 0; i < 4; ++i)
		_nmap[i] = NULL;
}

MateriaSource::MateriaSource(MateriaSource const & n)
{
	*this = n;
}

void 		MateriaSource::learnMateria(AMateria* m)
{
	int	t;

	t = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (t == 0 && _nmap[i] == NULL)
		{
			_nmap[i] = m;
			t++;
		}
	}
}

AMateria* 	MateriaSource::createMateria(std::string const & type)
{
	for (int i = 0; i < 4; ++i)
		if (_nmap[i] != NULL)
			if (_nmap[i]->getType() == type)
			{
				AMateria * n;
				n = _nmap[i]->clone();
				return (n);
			}
	return (NULL);
}

MateriaSource & MateriaSource::operator=(MateriaSource const & r)
{
	_nmap = r._nmap;
	return (*this);
}