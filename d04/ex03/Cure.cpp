/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:13:41 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.hpp"
#include <iostream>

Cure::Cure(void): AMateria("cure") {}

Cure::Cure(std::string const & type): AMateria("cure")
{
	(void)type;
}

Cure::Cure(Cure const & n): AMateria(n) {}

Cure::~Cure() {}

Cure*	Cure::clone(void) const
{
	Cure *	n = new Cure("cure");
	return (n);
}

void		Cure::use(ICharacter& target)
{
	setXP();
	std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
}