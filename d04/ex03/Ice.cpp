/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.cpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:13:53 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Ice.hpp"
#include <iostream>

Ice::Ice(): AMateria("ice") {}

Ice::Ice(std::string const & type): AMateria("ice")
{
	(void)type;
}

Ice::Ice(Ice const & n): AMateria(n) {}

Ice::~Ice() {}

Ice*	Ice::clone() const
{
	Ice *	n = new Ice("ice");
	return (n);
}

void		Ice::use(ICharacter& target)
{
	setXP();
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}