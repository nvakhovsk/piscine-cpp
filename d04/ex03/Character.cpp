/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 09:06:21 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/04 09:13:29 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.hpp"
#include <iostream>

Character::Character(void): _name("")
{
	_map = new AMateria*[4];
}

Character::Character(std::string name): _name(name)
{
	_map = new AMateria*[4];
	for (int i = 0; i < 4; ++i)
		_map[i] = NULL;
}

Character::Character(Character const & n)
{
	*this = n;
}

Character::~Character()
{
	delete [] _map;
}

std::string const & Character::getName() const
{
	return (_name);
}

void 				Character::equip(AMateria* m)
{
	int	t;

	t = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (t == 0 && _map[i] == NULL)
		{
			_map[i] = m;
			t++;
		}
	}
}

void 				Character::unequip(int idx)
{
	if (_map[idx] != NULL)
		_map[idx] = NULL;
}

void 				Character::use(int idx, ICharacter& target)
{
	if (_map[idx] == NULL || idx > 3 || idx < 0)
		;
	else
		{
			_map[idx]->use(target);
			_map[idx] = NULL;
		}
}

Character &	Character::operator=(Character const & r)
{
	_name = r._name;
	_map = r._map;
	return (*this);
}