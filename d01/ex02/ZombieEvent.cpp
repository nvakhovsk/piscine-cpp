/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 11:38:42 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <iostream>

ZombieEvent::ZombieEvent(void) {}

ZombieEvent::~ZombieEvent(void) {}

void		ZombieEvent::setZombieType(std::string type)
{
	_zombie_type = type;
}

Zombie*		ZombieEvent::newZombie(std::string name)
{
	Zombie*		z = new Zombie();

	z->setName(name);
	z->setType(_zombie_type);
	return (z);
}

Zombie*		ZombieEvent::randomChump(void)
{
	std::string		list[] = {"Vlad", "Natasha", "Vitya", "Vanya"};
	Zombie* 		z = newZombie(list[rand() % 4]);

	z->announce();
	return (z);
}