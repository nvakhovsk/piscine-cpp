/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 15:26:08 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <iostream>
#include <ctime>

int		main(void)
{
	Zombie			z1;
	Zombie			z2;
	ZombieEvent		event;

	std::srand(std::time(0));

	std::cout << std::endl << "------------- 2 zombies created------------------" << std::endl << "---------------------make them announce----------------------" << std::endl;

	z1.announce();
	z2.announce();

	std::cout << std::endl << "----------set different zombies type and create 3 random zombies on the heap---------------" << std::endl;

	event.setZombieType("1");
	Zombie*		newz1 = event.randomChump();
	event.setZombieType("2");
	Zombie*		newz2 = event.randomChump();
	Zombie*		newz3 = event.randomChump();

	std::cout << std::endl << "----------------------------------kill zombies on the heap---------------------------------" << std::endl;

	delete newz1;
	delete newz2;
	delete newz3;

	std::cout << std::endl << "-----------------------------------zombies on the stack dies-------------------------------" << std::endl;

	return 0;
}
