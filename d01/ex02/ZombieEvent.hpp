/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 11:58:17 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_H
# define ZOMBIEEVENT_H

#include "Zombie.hpp"
#include <iostream>

class ZombieEvent 
{

	private:
		std::string		_zombie_type;

	public:

		ZombieEvent(void);
		~ZombieEvent(void);

		void		setZombieType(std::string type);
		Zombie*		newZombie(std::string name);
		Zombie*		randomChump(void);
		std::string getZType(void);
		void		setZType(std::string t);

	
};

#endif
