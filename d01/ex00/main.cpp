// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/06 17:24:55 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/06 18:55:23 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "Pony.hpp"

void	ponyOnTheHeap(void)
{
	Pony*	heap = new Pony;

	std::cout << "A Heap Pony." << std::endl;
	delete heap;
}

void	ponyOnTheStack(void)
{
	Pony	stack;

	std::cout << "A Stack Pony." << std::endl;
}

int		main(void)
{
	std::cout << std::endl << "Call ponyOnTheHeap :" << std::endl;
	ponyOnTheHeap();
	std::cout << std::endl << "Call ponyOnTheStack :" << std::endl;
	ponyOnTheStack();
	std::cout << std::endl << "End." << std::endl;
	return 0;
}
