// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/06 17:17:39 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/06 18:14:13 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"
#include <iostream>

Pony::Pony(void)
{
	std::cout << "A Pony is born %)" << std::endl;
	return;
}

Pony::~Pony(void)
{
	std::cout << "A Pony is dead %(" << std::endl;
	return;
}
