/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 12:17:38 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int		main()
{
	std::string		s = "HI THIS IS BRAIN";
	std::string*	s_ptr = &s;
	std::string&	s_ref = s;

	std::cout << *s_ptr << std::endl;
	std::cout << s_ref << std::endl;
}
