/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 10:51:11 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

Zombie::Zombie(void): _name("Alice"), _type("Infected")
{
	std::cout << "A zombie is born." << std::endl;
}

Zombie::~Zombie(void)
{
	std::cout << "A zombie " << _name << " is dead." << std::endl;
}

void	Zombie::announce(void) 
{

	std::cout << "<" + _name + " (" + _type + ")> " + "Braiiiiiinnnnssss..." << std::endl;
	return ;
}

void	Zombie::setName(std::string n) 
{
	_name = n;
}

void	Zombie::setType(std::string t) 
{
	_type = t;
}

std::string	Zombie::getName() 
{
	return _name;
}

std::string	Zombie::getType() 
{
	return _type;
}