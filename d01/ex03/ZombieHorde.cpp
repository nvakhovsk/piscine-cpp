/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 08:27:38 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 11:43:44 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"
#include <iostream>

ZombieHorde::ZombieHorde(int n)
{
	std::string		list[] = {"Vlad", "Natasha", "Vitya", "Vanya"};
	_horde = new Zombie[n];

	_nmbr = n;
	for (int i = 0; i < n; i++)
		_horde[i].setName(list[rand() % 4]);
}

ZombieHorde::~ZombieHorde(void)
{
	delete [] _horde;
}

void	ZombieHorde::announce(void)
{
	for (int i = 0; i < _nmbr; i++)
		_horde[i].announce();
}
