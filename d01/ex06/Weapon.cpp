/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 13:38:40 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "Weapon.hpp"
#include <iostream>

Weapon::Weapon(void): _type("none") {}

Weapon::Weapon(std::string s)
{
	_type = s;
}

Weapon::~Weapon(void) {}

std::string const &	Weapon::getType()
{
	return (_type);
}

void				Weapon::setType(std::string s)
{
	_type = s;
}
