/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 13:38:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "HumanB.hpp"
#include <iostream>

HumanB::HumanB(std::string s1): _name(s1) {}

HumanB::~HumanB(void) {}

void	HumanB::attack(void)
{
	std::cout << _name + " attacks with his " + _weapon->getType() << std::endl;
}

void	HumanB::setWeapon(Weapon& new_weapon)
{
	_weapon = &new_weapon;
}
