/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 13:37:40 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_H
# define HUMANA_H

#include "Weapon.hpp"
#include <iostream>

class HumanA 
{
	private:
			std::string		_name;
			Weapon&			_weapon;

	public:

		HumanA(std::string s, Weapon& t);
		~HumanA(void);

		void	attack(void);	
};

#endif
