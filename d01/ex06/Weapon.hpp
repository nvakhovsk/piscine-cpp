/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 13:19:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 13:38:33 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_H
# define WEAPON_H

#include <iostream>

class Weapon 
{
	private:
		std::string		_type;

	public:

		Weapon();
		Weapon(std::string s);
		~Weapon();

		std::string const &	getType();
		void				setType(std::string s);
};

#endif
