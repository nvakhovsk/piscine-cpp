/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 15:28:31 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include <iostream>


struct Act
{
	std::string action_name;
	void (Human::*f)(std::string const &target);
};

void	Human::meleeAttack(std::string const & target)
{
	std::cout << "Use melee attack on " + target << std::endl;
}

void	Human::rangedAttack(std::string const & target)
{
	std::cout << "Use ranged attack on " + target << std::endl;

}

void	Human::intimidatingShout(std::string const & target)
{
	std::cout << "Use intimidating shout on " + target << std::endl;
}

void	Human::action(std::string const & action_name, std::string const & target)
{
	Act act[] = {
			{"is", &Human::intimidatingShout},
			{"ra", &Human::rangedAttack},
			{"ma", &Human::meleeAttack}
	};
	for (int i = 0; i < 3; i++)
		if (action_name == act[i].action_name)
		{
			(this->*act[i].f)(target);
			break;
		}
}

