/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 15:21:25 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

int		main()
{
	Human	alice;

	alice.action("ra", "Polikarp");
	alice.action("ma", "Lyubomir");
	alice.action("is", "Myslibor");
}