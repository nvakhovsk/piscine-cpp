/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 09:42:20 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/01 14:24:23 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>

std::string 	ft_capitalize(std::string s)
{
	std::string 	t;

	for (size_t i = 0; i < s.size(); i++)
		t += toupper(s[i]);
	return t;
}

void	ft_replace(char **argv)
{
	std::string 	s1;
	std::string 	s2;
	std::string 	newfile;
	std::ofstream	file2;
	std::ifstream	file1;
	std::streampos	size;

	newfile = ft_capitalize(argv[1]);
	newfile += ".replace";
	file2.open(newfile); //Opens the file identified by argument filename
	s1 = argv[2];
	s2 = argv[3];
	file1.open(argv[1]);
	if (file1.is_open()) //Returns whether the stream is currently associated to a file.
	{
		file1.seekg(0, std::ios::end); //Sets the position of the next character to be extracted from the input stream.
		size = file1.tellg(); //Returns the position of the current character in the input stream.
		std::string	buffer(size, ' ');
		file1.seekg(0, std::ios::beg);
		file1.read(&buffer[0], size);
		while (buffer.find(s1) != (size_t)-1)
			buffer.replace(buffer.find(s1), s1.length(), s2);
		file2 << buffer;
		file1.close();
	}
	else
		std::cout << "Error: Unable to open file." << std::endl;
}

int		main(int argc, char **argv)
{
	if (argc != 4)
		std::cout << "Error: usage: ./replace <Filename> <string1> <string2>" << std::endl;
	else
		ft_replace(argv);
	return (0);
}