/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   class_phonebook.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 09:54:25 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/31 12:48:14 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "class_phonebook.hpp"
#include <iomanip>
#include <string>

Phonebook::Phonebook(): fl(0) {}

Phonebook::~Phonebook() {}

int	Phonebook::ft_add(void)
{
	if (fl != 1)
	{
		fl = 1;
		std::cout << "Please, enter first name" << std::endl;
		std::getline (std::cin, first_name);
		std::cout << "Please, enter last name" << std::endl;
		std::getline (std::cin, last_name);
		std::cout << "Please, enter nickname" << std::endl;
		std::getline (std::cin, nickname);
		std::cout << "Please, enter login" << std::endl;
		std::getline (std::cin, login);
		std::cout << "Please, enter postal address" << std::endl;
		std::getline (std::cin, postal_address);
		std::cout << "Please, enter phone number" << std::endl;
		std::getline (std::cin, phone_number);
		std::cout << "Please, enter birthday date" << std::endl;
		std::getline (std::cin, birthday_date);
		std::cout << "Please, enter favorite meal" << std::endl;
		std::getline (std::cin, favorite_meal);
		std::cout << "Please, enter underwear color" << std::endl;
		std::getline (std::cin, underwear_color);
		std::cout << "Please, enter darkest secret" << std::endl;
		std::getline (std::cin, darkest_secret);
		return 1;
	}
	return 0;
}

void	Phonebook::print_table(void)
{
	char	buff[11];

	if (fl == 1)
	{
		std::cout << std::setfill (' ') << std::setw (10);
		if (first_name.size() > 10)
		{
			first_name.copy(buff, 9, 0);
			buff[9] = '.';
			buff[10] = '\0';
			std::cout << buff;
		}
		else
			std::cout << first_name;
		std::cout << "|";
		std::cout << std::setfill (' ') << std::setw (10);
		if (last_name.size() > 10)
		{
			last_name.copy(buff, 9, 0);
			buff[9] = '.';
			buff[10] = '\0';
			std::cout << buff;
		}
		else
			std::cout << last_name;
		std::cout << "|";
		std::cout << std::setfill (' ') << std::setw (10);
		if (nickname.size() > 10)
		{
			nickname.copy(buff, 9, 0);
			buff[9] = '.';
			buff[10] = '\0';
			std::cout << buff;
		}
		else
			std::cout << nickname;
		std::cout << std::endl;
	}
	else
	{
		std::cout << std::setfill (' ') << std::setw (11);
		std::cout << "|";
		std::cout << std::setfill (' ') << std::setw (11);
		std::cout << "|";
		std::cout << std::setfill (' ') << std::setw (11);
		std::cout << std::endl;
	}
	return ;
}

void	Phonebook::print_one(void)
{
	if (fl == 1)
	{
		std::cout << "First name: " << first_name << std::endl;
		std::cout << "Last name: " << last_name << std::endl;
		std::cout << "Nickname: " << nickname << std::endl;
		std::cout << "Login: " << login << std::endl;
		std::cout << "Postal Address: " << postal_address << std::endl;
		std::cout << "Email: " << email_address << std::endl;
		std::cout << "Phone Number: " << phone_number << std::endl;
		std::cout << "Birthday: " << birthday_date << std::endl;
		std::cout << "Favorite Meal: " << favorite_meal << std::endl;
		std::cout << "Underwear Color: " << underwear_color << std::endl;
		std::cout << "Darkest Secret: " << darkest_secret << std::endl;
	}
	else
		std::cout << "Error. Contact not found" << std::endl;
	return ;
}

