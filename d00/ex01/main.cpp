/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 09:37:13 by nmatushe          #+#    #+#             */
/*   Updated: 2017/10/31 12:39:13 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "class_phonebook.hpp"
#include <iomanip>
#include <string>

void	ft_add_one(Phonebook *list)
{
	if (list[7].fl == 0)
		for (int i = 0; list[i].ft_add() == 0; i++)
			;
	else
		std::cout << "Error. The phonebook is full." << std::endl;
	return ;
}

void	ft_search_one(Phonebook *list)
{
	std::string		str_index;
	int				index;

	std::cout << "   Index  |First name|Last name | Nickname " << std::endl;
	for (int i = 0; i < 7; i++)
	{
		if (list[i].fl == 1)
		{
			std::cout << std::setfill (' ') << std::setw (10);
			std::cout << i + 1 << "|";
			list[i].print_table();
		}
	}
	std::cout << "Please, enter index" << std::endl;
	std::getline (std::cin, str_index);
	if (str_index.size() == 1 && str_index[0] > 48 && str_index[0] < 57)
	{
		index = str_index[0] - 49;
		list[index].print_one();
	}
	else
		std::cout << "Error. Please enter a number (0..7)" << std::endl;
}

int		main(void)
{
	Phonebook		list[8];
	std::string		str;

	while (str != "EXIT")
	{
		std::cout << "Please, enter a command: " << std::endl;
		std::getline (std::cin, str);
		if (str == "ADD")
		{
			std::cout << "ADD new contact" << std::endl;
			ft_add_one(list);
		}
		else if (str == "SEARCH")
			ft_search_one(list);
		else if (str != "" && str != "EXIT")
			std::cout << "Error. Please enter only ADD, SEARCH or EXIT" << std::endl;
	}
	return 0;
}
