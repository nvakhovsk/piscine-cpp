// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 12:21:09 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/13 00:51:24 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include <iostream>

/*****************************Bureaucrat********************************/

Bureaucrat::Bureaucrat() : _name("unknown"), _grade(150) {}

Bureaucrat::Bureaucrat(const std::string name, int grade) : _name(name)
{
	if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else
		_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const & don) : _name(don._name)
{
	*this = don;
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat & Bureaucrat::operator=(Bureaucrat const & r)
{
	if (this != &r)
	{
		const_cast<std::string&>(this->_name) = r._name;
		_grade = r._grade;
	}
	return * this;
}

const std::string Bureaucrat::getName()
{
	return this->_name;
}

int	Bureaucrat::getGrade() const
{
	return this->_grade;
}

void Bureaucrat::executeForm(Form const & don) const
{
	if (this->getGrade() < don.getExecutionGrade())
	{
		std::cout << this->_name << " executes " << don.getTarget() << "." << std::endl;
		don.execute(*this);
	}
	else
		std::cout << "Cannot execute form because the bureaucrat's grade is too low." << std::endl;
}

void Bureaucrat::signForm(Form & don)
{
	if (don.getSign() == true)
		std::cout << _name << "cannot sign" << don.getName() << "because it has already been signed." << std::endl;
	else if (_grade <= don.getSignGrade())
	{
		don.setSign(true);
		std::cout << _name << " signs " << don.getName() << std::endl;
	}
	else
		std::cout << _name << "cannot sign" << don.getName() << "because his grade is too low." << std::endl;
}

Bureaucrat & Bureaucrat::operator+=(int amount)
{
	int newGrade = this->_grade - amount;
	if (newGrade < 1)
	{
		throw Bureaucrat::GradeTooHighException();
	}
	else
		this->_grade += amount;
    return * this;
}

Bureaucrat & Bureaucrat::operator-=(int amount)
{
	int newGrade = this->_grade + amount;
	if (newGrade > 150)
		throw Bureaucrat::GradeTooLowException();
	else
		this->_grade += amount;
    return * this;
}

std::ostream & operator<<(std::ostream & o, Bureaucrat & r)
{
	o << r.getName() << ", bureaucrat grade " << r.getGrade() << std::endl;
	return (o);
}

/*************************GradeTooHighException**********************/

Bureaucrat::GradeTooHighException::GradeTooHighException() {}

Bureaucrat::GradeTooHighException::GradeTooHighException(GradeTooHighException const & don)
{
	*this = don;
}

Bureaucrat::GradeTooHighException & Bureaucrat::
	GradeTooHighException::operator=(GradeTooHighException const & r)
{
	if (this != &r)
		return *this;
	return *this;
}

Bureaucrat::GradeTooHighException::
	~GradeTooHighException() throw()
{
}

const char * Bureaucrat::GradeTooHighException::what() const throw()
{
	return ("Grade is too high.");
}

/*************************GradeTooLowException**********************/

Bureaucrat::GradeTooLowException::GradeTooLowException() {}

Bureaucrat::GradeTooLowException & Bureaucrat::
	GradeTooLowException::operator=(GradeTooLowException const & r)
{
	if (this != &r)
		return *this;
	return *this;
}

Bureaucrat::GradeTooLowException::
	GradeTooLowException(GradeTooLowException const & don)
{
	*this = don;
}

Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {}

const char * Bureaucrat::GradeTooLowException::what() const throw()
{
	return ("Grade is too low.");
}
