// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 13:07:36 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/13 00:49:46 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <cstdlib>

int main()
{
	srand(time(NULL));


	std::cout << "/************ Forms execution ************\\" << std::endl;

	std::cout << std::endl << "-------------Create 2 Bureaucrat (Polikarp lvl 1 and Lybomyr lvl 77)-------------" <<std::endl;
	Bureaucrat t1 = Bureaucrat("Polikarp", 1);
	Bureaucrat t2 = Bureaucrat("Lybomyr", 77);

	std::cout << "-------------Create 3 forms with target names-------------" <<std::endl;
	PresidentialPardonForm PForm = PresidentialPardonForm("Onufriy");
	ShrubberyCreationForm SForm = ShrubberyCreationForm("Avreliy");
	RobotomyRequestForm RForm = RobotomyRequestForm("Myslibor");

	std::cout << std::endl << "-------------Execute Robot form (50% chances of success)-------------" <<std::endl;
	RForm.setSign(true);

	try
	{
		RForm.execute(t1);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-------------Execute Presidential form (not signed)-------------" <<std::endl;
	try
	{
		PForm.execute(t1);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-------------Sign and execute Presidential form-------------" <<std::endl;
	PForm.setSign(true);
	try
	{
		PForm.execute(t1);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-------------Sign and execute Shrubbery form-------------" <<std::endl;
	SForm.setSign(true);
	try
	{
		SForm.execute(t1);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << "(<target>_shrubbery file created)" << std::endl;

	std::cout << std::endl << "-------------Lybomyr fail to sign Presidential form-------------" <<std::endl;
	try
	{
		t2.executeForm(PForm);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-------------Polikarp execute Presidential form-------------" <<std::endl;
	try
	{
		t1.executeForm(PForm);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-------------Lybomyr fail to execute Shrubbery form-------------" <<std::endl;
    try
    {
		PForm.execute(t2);
    }
    catch (Form::GradeTooLowException e)
    {
		std::cout << e.what() << std::endl;
    }

	std::cout << std::endl << "-------------Lybomyr fail to execute Robot form-------------" <<std::endl;
    try
    {
		RForm.execute(t2);
    }
    catch (Form::GradeTooLowException e)
    {
		std::cout << e.what() << std::endl;
    }
	return (0);
}
