// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 13:44:10 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 21:27:32 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FORM_HPP
# define FORM_HPP

#include <iostream>
#include "Bureaucrat.hpp"

class Bureaucrat;

class Form
{
	private:
		const std::string 	_name;
		std::string 		_target;
		bool				_sign;
		const int			_signGrade;
		const int			_executeGrade;

	public:
		Form();
		Form(Form const & don);
		Form(std::string name, int signGrade, int executeGrade);
		virtual ~Form();

		void setSign(bool boolean);
		void setTarget(std::string target);

		bool 		getSign() const;
		std::string getTarget() const;
		std::string getName() const;
		int 		getSignGrade() const;
		int 		getExecutionGrade() const;

		virtual void execute(Bureaucrat const &) const = 0;

		void beSigned(Bureaucrat & bureaucrat);

		Form & operator=(Form const & r);
		
		class GradeTooHighException : public std::exception
		{
			public:
				GradeTooHighException();
				GradeTooHighException(GradeTooHighException const &);
				~GradeTooHighException() throw();

				GradeTooHighException &operator=(GradeTooHighException const &);
				
				virtual const char* what() const throw();
		};

		class GradeTooLowException : public std::exception
		{
			public:
				GradeTooLowException();
				GradeTooLowException(GradeTooLowException const &);
				~GradeTooLowException() throw();

				GradeTooLowException &operator=(GradeTooLowException const &);
		
				virtual const char* what() const throw();
		};
};

std::ostream & operator<<(std::ostream & o, Form & r);

#endif

