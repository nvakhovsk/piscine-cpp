// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RobotomyRequestForm.hpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 21:36:23 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 21:47:24 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef  ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

#include <iostream>
#include "Form.hpp"

class RobotomyRequestForm : public Form
{
	public:
	    RobotomyRequestForm();
	    RobotomyRequestForm(std::string target);
	    RobotomyRequestForm(RobotomyRequestForm const & don);
	    
	    ~RobotomyRequestForm();
	    
		void execute(Bureaucrat const & executor) const;

	    RobotomyRequestForm & operator=(RobotomyRequestForm const & r);
};

#endif
