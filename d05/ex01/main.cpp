// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: ndauteui <ndauteui@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 13:07:36 by ndauteui          #+#    #+#             //
//   Updated: 2017/11/06 18:53:39 by ndauteui         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <iostream>

int main()
{
    std::cout << "************ Bureaucrats ************" << std::endl;

   Bureaucrat t1 = Bureaucrat("Polikarp", 1);
    std::cout << t1;
    Bureaucrat t2 = Bureaucrat("Myslibot", 150);
    std::cout << t2;
    Bureaucrat t3 = Bureaucrat("Onufriy", 77);
    std::cout << t3;
    
    try
    {
        std::cout << std::endl << "-----------Adding 1 grade to Polikarp-----------" << std::endl;
        t1 += 1;
        std::cout << t1;
    }
    catch (Bureaucrat::GradeTooHighException e)
    {
        std::cout << e.what() << std::endl;
    }

    try
    {
        std::cout << std::endl << "-----------Degrading 1 grade from Myslibot-----------" << std::endl;
        t2 -= 1;
        std::cout << t2;
    }
    catch (Bureaucrat::GradeTooLowException e)
    {
        std::cout << e.what() << std::endl;
    }

    try
    {
        std::cout << std::endl << "-----------Adding 11 grade to Onufriy-----------" << std::endl;
        t3 += 11;
        std::cout << t3;
    }
    catch (Bureaucrat::GradeTooLowException e)
    {
        std::cout << e.what() << std::endl;
    }

    try
    {
        std::cout << std::endl << "-----------Trying to instanciate a bureaucrat with 0 grade-----------" << std::endl;
        Bureaucrat t4 = Bureaucrat("Lybomyr", 0);
        std::cout << t4;
    }
    catch (Bureaucrat::GradeTooHighException e)
    {
        std::cout << e.what() << std::endl;
    }

    try
    {
        std::cout << std::endl << "-----------Trying to instanciate a bureaucrat with 152 grade-----------" << std::endl;
        Bureaucrat t5 = Bureaucrat("Someone", 151);
        std::cout << t5;
    }
    catch (Bureaucrat::GradeTooLowException e)
    {
        std::cout << e.what() << std::endl;
    }

    std::cout << std::endl << "-----------Constructing copy of Onufriy-----------" << std::endl;
    Bureaucrat t6(t3);
    std::cout << t6;

    
    std::cout << std::endl << "************ Forms ************" << std::endl;
    
    Form Form1 = Form("Form", 111, 37);
    std::cout << Form1;
    
    Form Form2(Form1);
    std::cout << Form2;
    
    Form Form3 = Form();
    std::cout << Form3;
    
    try
    {
        t1.signForm(Form1);
    }
    catch (Form::GradeTooLowException e)
    {
        std::cout << e.what() << std::endl;
    }

    try
    {
 	   Form3.beSigned(t2);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
	    Form2.beSigned(t3);
	}
	catch (Form::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

    return (0);
}
