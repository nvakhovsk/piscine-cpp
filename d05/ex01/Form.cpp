// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 18:48:35 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 18:48:37 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"
#include <iostream>

Form::Form() : _name("unknown"),  _signed(false), _signGrade(7), _execGrade(11) {}

Form::Form(std::string name, int sign, int exec):
			_name(name), _signGrade(sign), _execGrade(exec) 
{
	if (sign > 150 || exec > 150)
		throw Form::GradeTooLowException();
	else if (sign < 1 || exec < 1)
		throw Form::GradeTooHighException();
	else
		this->_signed = 0;
}

Form::Form(Form const & don): 
	_name(don._name),
		_signGrade(don._signGrade),  _execGrade(don._execGrade)
{
	*this = don;
}

Form::~Form() {}

Form &	Form::operator=(Form const & r)
{
	const_cast<std::string&>(this->_name) = r._name;
	const_cast<int&>(this->_signGrade) = r._signGrade;
	const_cast<int&>(this->_signGrade) = r._execGrade;
	this->_signed = r._signed;
	return *this;
}

std::string		Form::getName() const
{
	return (this->_name);
}

bool	Form::getSigned() const
{
	return (this->_signed);
}

int		Form::getSignGrade() const
{
	return (this->_signGrade);
}

int		Form::getExecGrade() const 
{
	return (this->_execGrade);
}

void	Form::setSigned() 
{
	this->_signed = 1;
}

void	Form::beSigned(Bureaucrat & don)
{
	if (don.getGrade() > this->_signGrade)
	{
		throw Form::GradeTooLowException();
	}
	else if (this->_signed == 1)
		std::cout << don.getName() << " cannot sign "
					<< this->_name << " because the form is already signed." << std::endl;
	else
		std::cout << don.getName() << " signs " << this->_name << "." << std::endl;
		this->setSigned();
}

std::ostream &	operator<<(std::ostream & o, Form const & don) {

	o << don.getName() << ", Form sign require : grade "
		<< don.getSignGrade() << ", exec require : "
		<< don.getExecGrade() << ", signed status : " << don.getSigned() << std::endl;
	return o;
}


Form::GradeTooHighException::GradeTooHighException() {}

Form::GradeTooHighException::GradeTooHighException(GradeTooHighException const & don) {
	*this = don;
}

Form::GradeTooHighException::~GradeTooHighException() throw() {}

Form::GradeTooHighException &		Form::GradeTooHighException::operator=(GradeTooHighException const &)
{
	return *this;
}

const char* Form::GradeTooHighException::what() const throw() {
	return ("Error!! Grade is too high");
}

Form::GradeTooLowException::GradeTooLowException() {}

Form::GradeTooLowException::GradeTooLowException(GradeTooLowException const & don) {
	*this = don;
}

Form::GradeTooLowException::~GradeTooLowException() throw() {}

Form::GradeTooLowException &		Form::GradeTooLowException::operator=(GradeTooLowException const &)
{
	return *this;
}

const char* Form::GradeTooLowException::what() const throw() {
	return ("Error!! Grade is too low");
}