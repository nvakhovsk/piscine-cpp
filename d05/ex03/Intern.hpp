// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Intern.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 12:13:30 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 17:18:53 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef Intern_HPP
# define Intern_HPP

#include <iostream>
#include "Form.hpp"

class Form;

class Intern 
{

public:
	Intern(void);
	Intern(Intern const & don);
	~Intern(void);
	
	Intern & operator=(Intern const &);

	Form * makeForm(std::string form, std::string target);
};

#endif
