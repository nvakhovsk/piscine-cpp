// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 21:36:04 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 21:36:04 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm() : Form("Shrubbery", 145, 137) {}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form()
{
	this->setTarget(target);
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const & don) : Form()
{
	*this = don;
}

ShrubberyCreationForm::~ShrubberyCreationForm() {}

ShrubberyCreationForm & ShrubberyCreationForm::operator=(ShrubberyCreationForm const & r)
{
	if (this != &r)
		return *this;
	return *this;
}

void ShrubberyCreationForm::execute(Bureaucrat const & executor) const
{
	std::string name = this->getTarget() + "_shrubbery";
	const char *fileName = name.c_str();
	if (executor.getGrade() <= 137 && this->getSign() == true)
	{
		std::ofstream ofs(fileName);
		if (ofs)
		{
			ofs << 			"    |    " 
			<< std::endl << "   /|\\   " 
			<< std::endl << "  //|\\\\  " 
			<< std::endl << " ///|\\\\\\ " 
			<< std::endl << "////|\\\\\\\\" 
			<< std::endl << "    |    " 
			<< std::endl << " XXXXXXX " << std::endl;
		}
		ofs.close();
	}
	else if (this->getSign() == false)
		std::cout << "The form isn't signed yet." << std::endl;
	else
		throw Form::GradeTooLowException();
}