// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 13:07:36 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/13 00:49:46 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <cstdlib>

int main()
{
	std::cout << "-------------------Intern Creation-------------------" << std::endl << std::endl;

	Intern someRandomIntern;

	Form* rrf;
	Form* rsf;
	Form* rpf;
	
	rrf = someRandomIntern.makeForm("robotomy request", "Bender");
	std::cout << "Target: " << rrf->getTarget() << std::endl;
	rsf = someRandomIntern.makeForm("shrubbery creation", "Myslibor");
	std::cout << "Target: " << rsf->getTarget() << std::endl;
	rpf = someRandomIntern.makeForm("presidential pardon", "Onyfriy");
	std::cout << "Target: " << rpf->getTarget() << std::endl;

	delete rrf;
	delete rsf;
	delete rpf;

	return (0);
}
