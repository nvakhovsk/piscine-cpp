// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 12:13:30 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 17:18:53 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

#include <iostream>
#include "Form.hpp"

class Form;

class Bureaucrat 
{

private:
	Bureaucrat();
	const std::string 	_name;
	int				_grade;

public:
	Bureaucrat(const std::string name, int grade);
	Bureaucrat(Bureaucrat const & don);
	~Bureaucrat();
	const std::string getName();
	int	getGrade() const;
	void signForm(Form & don);
	void executeForm(Form const & form) const;

	Bureaucrat & operator=(Bureaucrat const & r);
	Bureaucrat & operator+=(int amount);
	Bureaucrat & operator-=(int amount);

	class GradeTooHighException : public std::exception
	{
	public:
		GradeTooHighException();
		GradeTooHighException(GradeTooHighException const & don);
		~GradeTooHighException() throw();
		GradeTooHighException & operator=(GradeTooHighException const & r);
		virtual const char * what() const throw();
	};

    class GradeTooLowException: public std::exception
    {
    public:
		GradeTooLowException();
		GradeTooLowException(GradeTooLowException const & don);
		~GradeTooLowException() throw();
		GradeTooLowException & operator=(GradeTooLowException const & r);
		virtual const char * what() const throw();
    };
};

std::ostream & operator<<(std::ostream & o, Bureaucrat & r);

#endif
