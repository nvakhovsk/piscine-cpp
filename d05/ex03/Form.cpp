// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 13:44:05 by nmatushe          #+#    #+#             //
//   Updated: 2015/01/13 00:48:18 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"
#include <iostream>

Form::Form() : 
	_name("Default"), _sign(false), _signGrade(55), _executeGrade(11) {}

Form::Form(std::string name, int signGrade, int executeGrade) : 
	_name(name), _sign(false), _signGrade(signGrade), _executeGrade(executeGrade) {}

Form::Form(Form const & don) : 
		_name(don._name), _sign(don._sign), 
		_signGrade(don._signGrade),  _executeGrade(don._executeGrade)
{
	*this = don;
}

Form::~Form() {}

Form & Form::operator=(Form const & r)
{
	if (this != &r)
	{
		const_cast<std::string&>(this->_name) = r._name;
		const_cast<int&>(this->_signGrade) = r._signGrade;
		const_cast<int&>(this->_signGrade) = r._executeGrade;
		this->_sign = r._sign;
	}
	return *this;
}

std::string Form::getName() const
{
	return this->_name;
}

bool Form::getSign() const
{
	return _sign;
}

int Form::getSignGrade() const
{
	return _signGrade;
}

int Form::getExecutionGrade() const
{
	return _executeGrade;
}

std::string Form::getTarget() const
{
	return this->_target;
}

void Form::setTarget(std::string target)
{
	_target = target;
}

void Form::setSign(bool boolean)
{
	_sign = boolean;
}

void Form::beSigned(Bureaucrat & bureaucrat)
{
	if (_sign != true)
	{
		if (bureaucrat.getGrade() <= this->getSignGrade())
		{
			this->setSign(true);
			std::cout << "The form " << _name
			<< " has been correctly signed by " << bureaucrat.getName() << std::endl;
		}
		else
		{
			throw Form::GradeTooLowException();
		}
	}
}

std::ostream & operator<<(std::ostream & o, Form & r)
{
	if (r.getSign() == true)
		o << "Form " << r.getName()
			<< ", signing grade required: " << r.getSignGrade()
			<< ", execution grade required:: " << r.getExecutionGrade()
			<< ", sign status: signed." << std::endl;
	else
		o << "Form " << r.getName()
			<< ", signing grade required: " << r.getSignGrade()
			<< ", execution grade required:: " << r.getExecutionGrade()
			<< ", sign status: waiting for signature." << std::endl;
	return o;
}

Form::GradeTooHighException::GradeTooHighException() {}

Form::GradeTooHighException::
	GradeTooHighException(GradeTooHighException const & don) 
{
	*this = don;
}

Form::GradeTooHighException::~GradeTooHighException() throw() {}

Form::GradeTooHighException &Form::
	GradeTooHighException::operator=(GradeTooHighException const &) 
{
	return *this;
}

const char* Form::GradeTooHighException::what() const throw() 
{
	return ("Form is too high");
}

Form::GradeTooLowException::GradeTooLowException() {}

Form::GradeTooLowException::
	GradeTooLowException(GradeTooLowException const & don) 
{
	*this = don;
}

Form::GradeTooLowException::~GradeTooLowException() throw() {}

Form::GradeTooLowException &Form::
	GradeTooLowException::operator=(GradeTooLowException const &) 
{
	return *this;
}

const char* Form::GradeTooLowException::what() const throw() 
{
	return("Grade is too low.");
}
