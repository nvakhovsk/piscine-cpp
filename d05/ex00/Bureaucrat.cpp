/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 09:23:19 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 15:59:04 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.hpp"

/*****************************Bureaucrat********************************/

Bureaucrat::Bureaucrat(): _name("unknown"), _grade(150) {}

Bureaucrat::Bureaucrat(std::string name, int grade): _name(name)
{

	if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
	else if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	else
		this->_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const & don): _name(don._name)
{
	*this = don;
}

Bureaucrat::~Bureaucrat() {}

Bureaucrat &	Bureaucrat::operator=(Bureaucrat const & r)
{
	const_cast<std::string&>(this->_name) = r._name;
	this->_grade = r._grade;
	return *this;
}

std::string		Bureaucrat::getName() const
{
	return (this->_name);
}


int		Bureaucrat::getGrade() const
{
	return (this->_grade);
}

void	Bureaucrat::operator-=(int const r)
{
	if ((this->_grade + r) > 150)
		throw Bureaucrat::GradeTooLowException();
	else
		this->_grade += r;
}

void	Bureaucrat::operator+=(int const r)
{
	if ((this->_grade - r) < 1)
		throw Bureaucrat::GradeTooHighException();
	else
		this->_grade -= r;
}

std::ostream &	operator<<(std::ostream & o, Bureaucrat const & don)
{
	o << don.getName() << ", bureaucrat grade "
		<< don.getGrade() << "." << std::endl;
	return o;
}

/*************************GradeTooHighException**********************/

Bureaucrat::GradeTooHighException::GradeTooHighException() {}

Bureaucrat::GradeTooHighException::
GradeTooHighException(GradeTooHighException const & don)
{
	*this = don;
}

Bureaucrat::GradeTooHighException::~GradeTooHighException() throw() {}

Bureaucrat::GradeTooHighException &		Bureaucrat::
GradeTooHighException::operator=(GradeTooHighException const &)
{
	return *this;
}

const char* Bureaucrat::GradeTooHighException::what() const throw()
{
	return ("Error! Bureaucrat grade too high!");
}

/*************************GradeTooLowException**********************/

Bureaucrat::GradeTooLowException::GradeTooLowException() {}

Bureaucrat::GradeTooLowException::
GradeTooLowException(GradeTooLowException const & don)
{
	*this = don;
}

Bureaucrat::GradeTooLowException::~GradeTooLowException() throw() {}

Bureaucrat::GradeTooLowException &		Bureaucrat::
GradeTooLowException::operator=(GradeTooLowException const &)
{
	return *this;
}

const char* Bureaucrat::GradeTooLowException::what() const throw()
{
	return ("Error! Bureaucrat grade  too low!");
}
