// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 14:22:53 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 14:22:54 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"

#include <iostream>

int main()
{
	Bureaucrat t1 = Bureaucrat("Polikarp", 1);
	std::cout << t1;
	Bureaucrat t2 = Bureaucrat("Myslibot", 150);
	std::cout << t2;
	Bureaucrat t3 = Bureaucrat("Onufriy", 77);
	std::cout << t3;
    
	try
	{
		std::cout << std::endl << "-----------Adding 1 grade to Polikarp-----------" << std::endl;
		t1 += 1;
		std::cout << t1;
	}
	catch (Bureaucrat::GradeTooHighException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << std::endl << "-----------Degrading 1 grade from Myslibot-----------" << std::endl;
		t2 -= 1;
		std::cout << t2;
	}
	catch (Bureaucrat::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << std::endl << "-----------Adding 11 grade to Onufriy-----------" << std::endl;
		t3 += 11;
		std::cout << t3;
	}
	catch (Bureaucrat::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << std::endl << "-----------Trying to instanciate a bureaucrat with 0 grade-----------" << std::endl;
		Bureaucrat t4 = Bureaucrat("Lybomyr", 0);
		std::cout << t4;
	}
	catch (Bureaucrat::GradeTooHighException e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << std::endl << "-----------Trying to instanciate a bureaucrat with 152 grade-----------" << std::endl;
		Bureaucrat t5 = Bureaucrat("Someone", 151);
		std::cout << t5;
	}
	catch (Bureaucrat::GradeTooLowException e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "-----------Constructing copy of Onufriy-----------" << std::endl;
	Bureaucrat t6(t3);
	std::cout << t6;

    return (0);
}
