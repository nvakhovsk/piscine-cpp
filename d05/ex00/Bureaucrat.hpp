// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2017/11/06 14:22:38 by nmatushe          #+#    #+#             //
//   Updated: 2017/11/06 14:22:39 by nmatushe         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

#include <iostream>

class Bureaucrat
{
	private:

		std::string const	_name;
		int					_grade;

	public:

		Bureaucrat();
		Bureaucrat(std::string name, int grade);
		Bureaucrat(Bureaucrat const &);
		~Bureaucrat();

		Bureaucrat &	operator=(Bureaucrat const &);
		void			operator+=(int const r);
		void			operator-=(int const r);

		std::string		getName() const;
		int				getGrade() const;

		class GradeTooHighException : public std::exception
		{
			public:
				GradeTooHighException();
				GradeTooHighException(GradeTooHighException const &);
				~GradeTooHighException() throw();

				GradeTooHighException &		operator=(GradeTooHighException const &);
				
				virtual const char* what() const throw();
		};

		class GradeTooLowException
		{
			public:
				GradeTooLowException();
				GradeTooLowException(GradeTooLowException const &);
				~GradeTooLowException() throw();

				GradeTooLowException &		operator=(GradeTooLowException const &);
				
				virtual const char* what() const throw();
			};
};
std::ostream &	operator<<(std::ostream &, Bureaucrat const &);

#endif
