/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 08:12:32 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/09 08:23:56 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template< typename T >
void	iter( T* array, unsigned int length, void (*f)(T) )
{
	for (unsigned int i = 0; i < length; ++i)
		f(array[i]);
}

template< typename T >
void	print(T var) {
	std::cout << var << std::endl;
}

int	main()
{
	int arr1[] = {34, 5, 76, 843562, 856, 4};
	std::string arr2[] = {"Myslibor", "Onyfriy", "Polykarp", "Vlad"};

	::iter(arr1, 6, ::print);
	::iter(arr2, 4, ::print);

	return 0;
}