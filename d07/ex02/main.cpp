/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 08:12:32 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/09 08:39:28 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Array.hpp"
#include <stdexcept>
#include <iostream>

int main()
{
	unsigned int i = 5;
	Array<int> intNum = Array<int>(i);

	intNum[0] = 6;
	intNum[1] = -6;
	intNum[2] = 56;
	intNum[3] = 234;
	intNum[4] = 132;
	try
	{
		intNum[5] = 6756;
	}
	catch (std::range_error e)
	{
		std::cout << e.what() << std::endl;
	}

	i = 6;
	Array<std::string> string = Array<std::string>(i);
	string[0] = "1 string";
	string[1] = "2 string";
	string[2] = "3 string";
	string[3] = "4 string";
	string[4] = "5 string";
	string[5] = "6 string";
	try
	{
		string[7] = "7 string";
	}
	catch (std::range_error e)
	{
		std::cout << e.what() << std::endl;
	}

	Array<int> def = Array<int>();
	std::cout << "Default constructor: " << def.size() << std::endl;

	return 0;
}