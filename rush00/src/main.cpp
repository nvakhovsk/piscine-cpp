#include "main.hpp"




GameManeger gameManeger;
bool gameOn = true;

int main()
{
	Window window;
	Player player; 

	window.setUnitOnMap(player);
	
	srand((unsigned int)time(NULL));

	window.printMap();

	window.pauseEvent();
	mvprintw(gYMap + 2, 22, "'w' - step up");
    mvprintw(gYMap + 4, 22, "'s' - step down");
    mvprintw(gYMap + 6, 22, "'a' - step left");
    mvprintw(gYMap + 8, 22, "'d' - step right");
    mvprintw(gYMap + 5, 52, "'SPACE' - shooting");

	while (window.getIsGameOn() && gameOn)
	{

		window.keyEvent(player);




		gameManeger.update(player, window._map);

		gameManeger.setEnemyInArray(window);


		window.printMap();


	}

	if (!gameOn)
	{
		int i = 10;
	    mvprintw(++i, 20, "_____.___.________   ____ ___     _____ _____________________ ");
	    mvprintw(++i, 20, "\\__  |   |\\_____  \\ |    |   \\   /  _  \\\\______   \\_   _____/ ");
	    mvprintw(++i, 20, "/   |   | /   |   \\|    |   /  /  /_\\  \\|       _/|    __)_ ");
	    mvprintw(++i, 20, " \\____   |/    |    \\    |  /  /    |    \\    |   \\|        \\  ");
	    mvprintw(++i, 20, "/ ______|\\_______  /______/   \\____|__  /____|_  /_______  /");
	    mvprintw(++i, 20, " \\/               \\/                   \\/       \\/        \\/ ");
	    mvprintw(++i, 20, ".____    ________   ________    ______________________________ ");
	    mvprintw(++i, 20, "|    |   \\_____  \\  \\_____  \\  /   _____/\\_   _____/\\______   \\ ");
	    mvprintw(++i, 20, "|    |    /   |   \\  /   |   \\ \\_____  \\  |    __)_  |       _/");
	    mvprintw(++i, 20, "|    |___/    |    \\/    |    \\/        \\ |        \\ |    |   \\");
	    mvprintw(++i, 20, "|_______ \\_______  /\\_______  /_______  //_______  / |____|_  /");
	    mvprintw(++i, 20, "        \\/       \\/         \\/        \\/         \\/         \\/ ");
	}
	


	return 0;
}


