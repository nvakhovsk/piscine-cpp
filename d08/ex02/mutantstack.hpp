/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 13:34:16 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/10 13:34:24 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stack>

#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

template <typename T>
class MutantStack : public std::stack<T>
{
	private:
		typedef    std::stack<T> base;
	public:
		MutantStack(void) {}
		~MutantStack(void) {}
		MutantStack(MutantStack const & o): base(o) {}
		typedef typename base::container_type::iterator iterator;
		using		base::operator=;

		iterator	begin() {return base::c.begin();}
		iterator	end() {return base::c.end();}
};

#endif