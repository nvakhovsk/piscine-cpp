/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 13:09:06 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/10 13:09:32 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
# define SPAN_HPP

#include <vector>

class Span
{
	private:	
		std::vector<int>	_v;
	public:

		Span();
		Span(unsigned int n);
		Span(Span const &);
		~Span();
		Span &	operator=(Span const &);

		void	addNumber(int n);
		int		shortestSpan();
		int		longestSpan();
};

#endif