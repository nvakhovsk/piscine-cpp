/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 13:09:06 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/10 13:13:47 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <numeric>

#include <iostream>

Span::Span()
{
	_v.reserve(0);
}

Span::Span(unsigned int n)
{
	_v.reserve(n);
}

Span::Span(Span const & don)
{
	*this = don;
}

Span::~Span() {}

Span &	Span::operator=(Span const & r)
{
	this->_v = r._v;
	return *this;
}

void	Span::addNumber(int n)
{
	if (this->_v.size() < this->_v.capacity())
		this->_v.push_back(n);
	else
		throw std::overflow_error("Error! container is full.");
}

int	Span::shortestSpan()
{
	std::vector<int> 	n = _v;

    std::sort(n.begin(), n.end());
	std::adjacent_difference(n.begin(), n.end(), n.begin() - 1);
	n.pop_back();
    return (n[std::distance(n.begin(), std::min_element(n.begin(), n.end()))]);
}

int	Span::longestSpan()
{
	std::vector<int>::iterator	min = std::min_element(_v.begin(), _v.end());
	std::vector<int>::iterator	max = std::max_element(_v.begin(), _v.end());

	return (_v[std::distance(_v.begin(), max)] - _v[std::distance(_v.begin(), min)]);
}
