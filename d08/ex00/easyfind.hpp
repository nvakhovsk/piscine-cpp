/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:14:39 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/10 12:18:07 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

#include <stdexcept>
#include <algorithm>
#include <iostream>

template < typename T >
int		easyfind(T & x, int y)
{
	typename T::iterator	iter = std::find(x.begin(), x.end(), y);
	if (iter != x.end())
			return *iter;
	throw std::logic_error("No element found!");
}

#endif