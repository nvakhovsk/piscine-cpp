/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:14:39 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/10 12:17:32 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"
#include <list>
#include <vector>

int main()
{
	std::list<int>	list1;

	list1.push_back(7);
	list1.push_back(45);
	list1.push_back(23);
	list1.push_back(567);
	list1.push_back(33);

	try
	{
		std::cout << easyfind(list1, 23) << std::endl;
	}
	catch (std::logic_error e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << easyfind(list1, 5) << std::endl;
	}
	catch (std::logic_error e)
	{
		std::cout << e.what() << std::endl;
	}

	std::vector<int>	vect;

	vect.push_back(45);
	vect.push_back(6);
	vect.push_back(43);
	vect.push_back(23);
	vect.push_back(56);

	try
	{
		std::cout << easyfind(vect, 43) << std::endl;
	}
	catch (std::logic_error e)
	{
		std::cout << e.what() << std::endl;
	}

	try
	{
		std::cout << easyfind(vect, 47) << std::endl;
	}
	catch (std::logic_error e)
	{
		std::cout << e.what() << std::endl;
	}

	return 0;
}