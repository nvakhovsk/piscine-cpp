/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 10:19:12 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <ctime>

struct SerData
{
	char	ser1[9];
	char	ser2[9];
	int 	n;
	
};

struct Data
{
	std::string ser1;
	int n;
	std::string ser2;
};

void *	serialize(void)
{
	SerData	*	arr = new SerData;

	int i = -1;
	while (++i < 8)
		while (!isalnum(arr->ser1[i]))
			arr->ser1[i] = (char)((rand() % 74) + 48);
	arr->ser1[i] = '\0';
	arr->n = rand() % 9 + 48;
	i = -1;
	while (++i < 8)
		while (!isalnum(arr->ser2[i]))
			arr->ser2[i] = (char)((rand() % 74) + 48);
	arr->ser2[i] = '\0';
	return reinterpret_cast<void *>(arr);
}

Data * 	deserialize(void * p)
{
	SerData *	arr;
	Data *		data = new Data;

	arr = reinterpret_cast<SerData *>(p);
	data->ser1 = static_cast<std::string>(arr->ser1);
	data->n = arr->n;
	data->ser2 = static_cast<std::string>(arr->ser2);
	return (data);
}

int 	main(void)
{
	void *		t;
	Data * 		p;

	std::srand(std::time(0));
	t = serialize();
	p = deserialize(t);
	std::cout << p->ser1 << std::endl;
	std::cout << p->n << std::endl;
	std::cout << p->ser2 << std::endl;
	return 0;
}
