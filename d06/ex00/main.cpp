/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 09:44:34 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstdlib>
#include <iostream>

void	convert_char(char *a)
{
	std::cout << "char : ";
	if (isalpha(a[0]))
	{
		std::cout << "impossible" << std::endl;
		return ;
	}
	 if (atoi(a) >= 128 || atoi(a) < 32)
        std::cout << "Non Displayable" << std::endl;
    else
        std::cout << static_cast<char>(atoi(a)) << std::endl;
}

void	convert_int(int nb, char *a)
{
	int i;

	std::cout << "int: ";
	if (isalpha(a[0]))
	{
		std::cout << "impossible" << std::endl;
		return ;
	}
	i = static_cast<char>(nb);	
	if ((i > -2147483648) && (i < 2147483647))
		std::cout << i << std::endl;
	else
		std::cout << "impossible" << std::endl;
}

void		convert_float(float fl, char *a)
{
	float f;

	std::cout << "float: ";
	if (isalpha(a[0]))
	{
		std::cout << a << "f" << std::endl;
		return ;
	}
	f = static_cast<float>(fl);
	std::cout << f;
	if (f >= 0)
		std::cout << ".0f" << std::endl;
	else 
		std::cout << "f" << std::endl;

}
void		convert_double(float fl, char *a)
{
	double d;

	std::cout << "double: ";
	if (isalpha(a[0]))
	{
		std::cout << a << std::endl;
		return ;
	}
	d = static_cast<char>(fl);
	std::cout << d << ".0" << std::endl;
}

int 	main( int ac, char **av )
{
	if (ac != 2)
	{
		std::cout << "ERROR" << std::endl;
		return (0);
	}
	int nb;
	float fl;

	nb = atoi(av[1]);
	fl = atof(av[1]); 
	convert_char(av[1]);
	convert_int(nb, av[1]);
	convert_float(fl, av[1]);
	convert_double(fl, av[1]);
	
	return 0;
}