/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 10:23:51 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "C.hpp"

C::C()
{
	std::cout << "C constructed" << std::endl;
}

C::C(C const & don)
{
	*this = don;
}
	
C::~C()
{
	std::cout << "C destructed" << std::endl;
}

C & C::operator=(C const & r)
{
	if (this != &r)
		*this = r;
	return *this;
}