/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   B.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 10:23:42 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "B.hpp"

B::B()
{
	std::cout << "B constructed" << std::endl;
}

B::B(B const & don)
{
	*this = don;
}
	
B::~B()
{
	std::cout << "B destructed" << std::endl;
}

B & B::operator=(B const & r)
{
	if (this != &r)
		*this = r;
	return *this;
}