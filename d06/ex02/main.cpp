/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 10:26:32 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"
#include <time.h>
#include <iostream>

void	identify_from_pointer(Base * p)
{
	if (dynamic_cast<A *>(p) != NULL)
		std::cout << "A" << std::endl;
	else if (dynamic_cast<B *>(p) != NULL)
		std::cout << "B" << std::endl;
	else if (dynamic_cast<C *>(p) != NULL)
		std::cout << "C" << std::endl;
}

void	identify_from_reference(Base & p)
{
	if (dynamic_cast<A *>(&p) != NULL)
		std::cout << "A" << std::endl;
	else if (dynamic_cast<B *>(&p) != NULL)
		std::cout << "B" << std::endl;
	else if (dynamic_cast<C *>(&p) != NULL)
		std::cout << "C" << std::endl;			
}

int	main(void)
{
	Base	*p;

	srand(time(NULL));

	int		r = (rand() % 3) + 1;

	if (r == 1)
		p = new A();
	else if (r == 2)
		p = new B();
	else if (r == 3)
		p = new C();
	else
		p = (NULL);

	std::cout << "Identify from pointer:" << std::endl;
	identify_from_pointer(p);
	std::cout << "Identify from reference:" << std::endl;
	identify_from_reference(*p);
	
	delete p;
	return (0);
}