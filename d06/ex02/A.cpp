/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   A.cpp                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 10:22:56 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "A.hpp"

A::A()
{
	std::cout << "A constructed" << std::endl;
}

A::A(A const & don)
{
	*this = don;
}
	
A::~A()
{
	std::cout << "A destructed" << std::endl;
}

A & A::operator=(A const & r)
{
	if (this != &r)
		*this = r;
	return *this;
}