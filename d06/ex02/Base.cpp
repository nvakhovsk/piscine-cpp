/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Base.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 09:44:22 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/08 09:45:10 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Base.hpp"

Base::Base(){}

Base::Base(Base const & don)
{
	*this = don;
}
	
Base::~Base(){}

Base & Base::operator=(Base const & r)
{
	if (this != &r)
		*this = r;
	return *this;
}