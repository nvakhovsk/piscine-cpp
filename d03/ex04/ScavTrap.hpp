/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:36:11 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include <iostream>

#include "ClapTrap.hpp"
#include "FragTrap.hpp"

class ScavTrap : public virtual ClapTrap 
{
	public:

		ScavTrap(void);
	    ScavTrap(std::string const name);
		ScavTrap(ScavTrap const & c);
	    ~ScavTrap(void);

		ScavTrap &	operator=(ScavTrap const & c);
		void		rangedAttack(std::string const & target);
		void		meleeAttack(std::string const & target);
		void		challengeNewcomer();
};

#endif
