/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:17:14 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.hpp"
#include <iostream>

SuperTrap::SuperTrap(void): FragTrap(), NinjaTrap() 
{
	_hit_points = 100;
	_max_hit_points = 100;
	_energy_points = 120;
	_max_energy_points = 120;
	_level = 1;
	_melee_attack_damage = 60;
	_ranged_attack_damage = 20;
	_armor_damage_reduction = 5;
	std::cout << "Constucted Super Trap" << std::endl;
	setName("Default");
}

SuperTrap::SuperTrap(std::string const name): FragTrap(name), NinjaTrap(name) 
{
	_hit_points = 100;
	_max_hit_points = 100;
	_energy_points = 120;
	_max_energy_points = 120;
	_level = 1;
	_melee_attack_damage = 60;
	_ranged_attack_damage = 20;
	_armor_damage_reduction = 5;
	std::cout << "Constucted Super Trap with name" << std::endl;
	setName(name);
}

SuperTrap::SuperTrap(SuperTrap const & c): FragTrap(c), NinjaTrap(c) 
{
	std::cout << "Copied Super Trap" << std::endl;
	*this = c;
}

SuperTrap::~SuperTrap() 
{
	std::cout << "Destructed Super Trap" << std::endl;
}

SuperTrap &  SuperTrap::operator=(SuperTrap const & c) 
{
	std::cout << "Holyyyyyy mooooooly!" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void	SuperTrap::rangedAttack(std::string const & target) 
{
	FragTrap::rangedAttack(target);
}

void	SuperTrap::meleeAttack(std::string const & target) 
{
	NinjaTrap::rangedAttack(target);
}
