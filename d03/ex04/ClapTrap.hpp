/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 13:45:03 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:47:36 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>

class ClapTrap 
{
	protected:
		std::string	_name;
		int			_hit_points;
		int			_max_hit_points;
		int			_energy_points;
		int			_max_energy_points;
		int			_level;
		int			_melee_attack_damage;
		int			_ranged_attack_damage;
		int			_armor_damage_reduction;
	public:

		ClapTrap(void);
	    ClapTrap(std::string const name);
		ClapTrap(ClapTrap const & c);
	    virtual ~ClapTrap(void);

		ClapTrap &		operator=(ClapTrap const & c);
		virtual void			rangedAttack(std::string const & target);
		virtual void			meleeAttack(std::string const & target);
		virtual void			takeDamage(unsigned int amount);
		virtual void			beRepaired(unsigned int amount);
		void			setName(std::string const name);
		std::string		getName(void) const;
};

#endif
