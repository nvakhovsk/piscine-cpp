/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:11:18 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>
#include <cstdlib>

ScavTrap::ScavTrap(void): ClapTrap()
{
	_energy_points = 50;
	_max_energy_points = 50;
	_melee_attack_damage = 20;
	_ranged_attack_damage = 15;
	_armor_damage_reduction = 3;
	std::cout << "Let's get this party started!" << std::endl;
	setName("Default");
}

ScavTrap::ScavTrap(std::string const name): ClapTrap(name)
{
	_energy_points = 50;
	_max_energy_points = 50;
	_melee_attack_damage = 20;
	_ranged_attack_damage = 15;
	_armor_damage_reduction = 3;
	std::cout << "This time it'll be awesome, I promise!" << std::endl;
	setName(name);
}

ScavTrap::ScavTrap(ScavTrap const & c): ClapTrap(c) 
{
	std::cout << "Look out everybody! Things are about to get awesome!" << std::endl;
	*this = c;
}

ScavTrap::~ScavTrap() {

	std::cout << "No, nononono NO! I'm too pretty to die!" << std::endl;
}

ScavTrap &  ScavTrap::operator=(ScavTrap const & c)
{
	std::cout << "Holy moly!" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void	ScavTrap::rangedAttack(std::string const & target)
{
	std::cout << "SC4V-TP " << this->getName() << " use " << target << " at range, and " << this->_ranged_attack_damage << " points of damage!" << std::endl;
}

void	ScavTrap::meleeAttack(std::string const & target) 
{

	std::cout << "SC4V-TP " << this->getName() << " use " << target << " at melee, and " << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void	ScavTrap::challengeNewcomer() 
{

	std::string		Challenge[] = {"1 challenge", "2 challenge", "3 challenge", "4 challenge", "5 challenge"};

	if (this->_energy_points > 0) 
	{
		this->_energy_points -= 25;
		std::cout << "SC4V-TP " << this->_name << " tries to " << Challenge[(rand() % 5)] << " , he has " << this->_energy_points << " energy points." << std::endl;
	} 
	else 
		std::cout << "SC4V-TP " << this->_name << " didn't panic." << std::endl;
}
