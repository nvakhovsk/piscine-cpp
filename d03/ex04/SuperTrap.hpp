/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 14:11:57 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:12:01 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP

#include <iostream>

#include "NinjaTrap.hpp"
#include "FragTrap.hpp"

class SuperTrap : public FragTrap, public NinjaTrap 
{
	public:

		SuperTrap(void);
	    SuperTrap(std::string const name);
		SuperTrap(SuperTrap const & c);
	    ~SuperTrap(void);

		void		rangedAttack(std::string const & target);
		void		meleeAttack(std::string const & target);
		SuperTrap &	operator=(SuperTrap const &);
};

#endif
