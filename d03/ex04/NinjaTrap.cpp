/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:05:14 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"
#include <iostream>

NinjaTrap::NinjaTrap(void): ClapTrap() 
{
	_hit_points = 60;
	_max_hit_points = 60;
	_energy_points = 120;
	_max_energy_points = 120;
	_level = 1;
	_melee_attack_damage = 60;
	_ranged_attack_damage = 5;
	_armor_damage_reduction = 0;
	std::cout << "A ninja constructed!" << std::endl;
	setName("Default");
}

NinjaTrap::NinjaTrap(std::string const name): ClapTrap(name) 
{
	_hit_points = 60;
	_max_hit_points = 60;
	_energy_points = 120;
	_max_energy_points = 120;
	_level = 1;
	_melee_attack_damage = 60;
	_ranged_attack_damage = 5;
	_armor_damage_reduction = 0;
	std::cout << "A ninja with name constructed!" << std::endl;
	setName(name);
}

NinjaTrap::NinjaTrap(NinjaTrap const & c): ClapTrap(c) 
{
	std::cout << "A ninja was copied!" << std::endl;
	*this = c;
}

NinjaTrap::~NinjaTrap() {

	std::cout << ".......A ninja died........" << std::endl;
}

NinjaTrap &  NinjaTrap::operator=(NinjaTrap const & c)
{
	std::cout << "Hnngh! Initialising!" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void	NinjaTrap::ninjaShoebox(FragTrap const & target) 
{
	std::cout << this->getName() << " use Frag on " << target.getName() << std::endl;
}

void	NinjaTrap::ninjaShoebox(ScavTrap const & target) 
{
	std::cout << this->getName() << " use Scav on " << target.getName() << std::endl;
}

void	NinjaTrap::ninjaShoebox(NinjaTrap const & target) 
{
	std::cout << this->getName() << " use shadows on " << target.getName() << std::endl;
}
