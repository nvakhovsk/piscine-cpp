/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 13:50:10 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:34:56 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

#include <iostream>

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

class NinjaTrap : public virtual  ClapTrap 
{
	public:
			NinjaTrap(void);
		    NinjaTrap(std::string const name);
			NinjaTrap(NinjaTrap const & c);
		    virtual ~NinjaTrap(void);

			NinjaTrap &		operator=(NinjaTrap const & c);
			void			ninjaShoebox(FragTrap const & target);
			void			ninjaShoebox(ScavTrap const & target);
			void			ninjaShoebox(NinjaTrap const & target);
};

#endif
