/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 14:18:26 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:52:27 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstdlib>
#include <iostream>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "ClapTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int		main() 
{
	std::cout << 				"----------FragTrap robot--------" << std::endl;
	std::cout << 				"------Create default robot------" << std::endl;
	FragTrap	r1("Polikarp");
	std::cout << 				"-------Create copy robot--------" << std::endl;
	FragTrap	r2(r1);

	srand(time(NULL));
	r2.setName("Alice");

	std::cout << std::endl << 	"----Fight with basic attacks----" << std::endl;
	r1.rangedAttack("Alice");
	r2.takeDamage(20);
	r1.meleeAttack("Alice");
	r2.takeDamage(30);

	std::cout << std::endl << 	"-----Kill one of the robots-----" << std::endl;
	r2.takeDamage(105);
	r2.takeDamage(105);

	std::cout << std::endl << 	"-----Heal one of the robots-----" << std::endl;
	r2.beRepaired(100);
	r2.beRepaired(100);

	std::cout << std::endl << 	"-------Use random attacks-------" << std::endl;
	r2.vaulthunter_dot_exe("Polikarp");
	r2.vaulthunter_dot_exe("Polikarp");
	r2.vaulthunter_dot_exe("Polikarp");
	r2.vaulthunter_dot_exe("Polikarp");
	r2.vaulthunter_dot_exe("Polikarp");
	r1.takeDamage(1);

	std::cout << 				"----------ScavTrap robot--------" << std::endl;
	ScavTrap	r3("Lybomyr");
	ScavTrap	r4(r3);

	srand(time(NULL));
	r4.setName("Myslibor");

	std::cout << std::endl << 	"----Fight with basic attacks----" << std::endl;
	r3.rangedAttack("Myslibor");
	r4.takeDamage(15);
	r3.meleeAttack("Myslibor");
	r4.takeDamage(20);

	std::cout << std::endl << 	"-----Kill one of the robots-----" << std::endl;
	r4.takeDamage(103);
	r4.takeDamage(103);

	std::cout << std::endl << 	"-----Heal one of the robots-----" << std::endl;
	r4.beRepaired(100);
	r4.beRepaired(100);

	std::cout << std::endl << 	"------Try random challenge------" << std::endl;
	r4.challengeNewcomer();
	r4.challengeNewcomer();
	r4.challengeNewcomer();

	std::cout << 				"--------------Ninja-------------" << std::endl;
	NinjaTrap	r5("Epistaphiy");
	NinjaTrap	r6("Onufriy");

	std::cout << std::endl << 	"--------Use ninjaShoebox--------" << std::endl;
	r5.ninjaShoebox(r1);
	r5.ninjaShoebox(r3);
	r5.ninjaShoebox(r6);

	std::cout << 				"--------------SUPER-------------" << std::endl;
	SuperTrap	r7("Zaebal");
	SuperTrap	r8("Ichtiandr");

	std::cout << std::endl << 	"----------Use attacks-----------" << std::endl;
	r7.rangedAttack("Myslibor");
	r7.meleeAttack("Myslibor");

	std::cout << std::endl << 	"--------Use ninjaShoebox--------" << std::endl;
	r7.ninjaShoebox(r1);
	r7.ninjaShoebox(r3);
	r7.ninjaShoebox(r6);

	std::cout << std::endl << 	"-------Use random attacks-------" << std::endl;
	r8.vaulthunter_dot_exe("Alice");
	r8.vaulthunter_dot_exe("Alice");
	r8.vaulthunter_dot_exe("Alice");
	r8.vaulthunter_dot_exe("Alice");
	r8.vaulthunter_dot_exe("Alice");

	std::cout << std::endl << 	"---------------End---------------" << std::endl;
	return 0;
}
