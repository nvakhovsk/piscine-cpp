/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 13:30:27 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 14:52:55 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include <iostream>
#include <cstdlib>

ClapTrap::ClapTrap(void): _hit_points(100), _max_hit_points(100), _level(1)
{
	std::cout << "Clap Trap constructor" << std::endl;
	setName("Default");
}

ClapTrap::ClapTrap(std::string const name): _hit_points(100), _max_hit_points(100), _level(1)
{
	std::cout << "Clap Trap constructor (name)" << std::endl;
	setName(name);
}

ClapTrap::ClapTrap(ClapTrap const & n)
{
	std::cout << "Clap Trap copy constructor" << std::endl;
	*this = n;
}

ClapTrap::~ClapTrap()
{
	std::cout << "Clap Trap destructor" << std::endl;
}

void	ClapTrap::setName(std::string const name) 
{
	this->_name = name;
}

std::string		ClapTrap::getName(void) const 
{
	return (this->_name);
}

ClapTrap &  ClapTrap::operator=(ClapTrap const & c) 
{
	std::cout << "Clap Trap initialising" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void    ClapTrap::takeDamage(unsigned int amount) {

	int		hp;

	if (this->_hit_points == 0)
	{
		std::cout << "CL4P-TP " << this->getName() << " hasn't hit points" << std::endl;
	}
	else if ((int)amount <= this->_armor_damage_reduction) 
	{
		std::cout << "CL4P-TP " << this->getName() << " didn't feel anything!" << std::endl;
	}
	else 
	{
		if ((hp = this->_hit_points + this->_armor_damage_reduction - amount) < 0)
			hp = 0;
		std::cout << "CL4P-TP " << this->getName() << " take " << amount << " damage, and has now " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}

void	ClapTrap::beRepaired(unsigned int amount) 
{
int		hp;

	if (this->_hit_points == this->_max_hit_points)
	{
		std::cout << "CL4P-TP "<< this->getName() << " has full health!" <<  std::endl;
	} 
	else 
	{
		if ((hp = this->_hit_points + amount) > 100)
			hp = 100;
		std::cout << "CL4P-TP " << this->getName() << " is repaired by " << amount << " points, and has now " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}
