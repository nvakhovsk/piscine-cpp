/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 12:54:00 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <cstdlib>
#include <iostream>

int		main() {

	std::cout << 				"------Create default robot------" << std::endl;
	FragTrap	r1("Bob");
	std::cout << 				"-------Create copy robot--------" << std::endl;
	FragTrap	r2(r1);

	srand(time(NULL));
	r2.setName("Alice");

	std::cout << std::endl << 	"----Fight with basic attacks----" << std::endl;
	r1.rangedAttack("Alice");
	r2.takeDamage(20);
	r1.meleeAttack("Alice");
	r2.takeDamage(30);

	std::cout << std::endl << 	"-----Kill one of the robots-----" << std::endl;
	r2.takeDamage(105);
	r2.takeDamage(105);

	std::cout << std::endl << 	"-----Heal one of the robots-----" << std::endl;
	r2.beRepaired(100);
	r2.beRepaired(100);

	std::cout << std::endl << 	"-------Use random attacks-------" << std::endl;
	r2.vaulthunter_dot_exe("Bob");
	r2.vaulthunter_dot_exe("Bob");
	r2.vaulthunter_dot_exe("Bob");
	r1.takeDamage(1);

	std::cout << std::endl << 	"---------------End---------------" << std::endl;
	return 0;
}
