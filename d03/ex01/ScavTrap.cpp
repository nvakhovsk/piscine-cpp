/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 13:31:46 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"
#include <iostream>
#include <cstdlib>

ScavTrap::ScavTrap(void): _hit_points(100), _max_hit_points(100), _energy_points(50), _max_energy_points(50), _level(1), _melee_attack_damage(20), _ranged_attack_damage(15), _armor_damage_reduction(3) 
{
	std::cout << "Let's get this party started!" << std::endl;
	setName("Default");
}

ScavTrap::ScavTrap(std::string const name): _hit_points(100), _max_hit_points(100), _energy_points(50), _max_energy_points(50), _level(1), _melee_attack_damage(20), _ranged_attack_damage(15), _armor_damage_reduction(3)  
{
	std::cout << "This time it'll be awesome, I promise!" << std::endl;
	setName(name);
}

ScavTrap::ScavTrap(ScavTrap const & c) 
{
	std::cout << "Look out everybody! Things are about to get awesome!" << std::endl;
	*this = c;
}

ScavTrap::~ScavTrap() 
{
	std::cout << "No, nononono NO! I'm too pretty to die!" << std::endl;
}

void	ScavTrap::setName(std::string const name) 
{
	this->_name = name;
}

std::string		ScavTrap::getName(void) const 
{
	return (this->_name);
}

ScavTrap &  ScavTrap::operator=(ScavTrap const & c) 
{
	std::cout << "Holy moly!" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void	ScavTrap::rangedAttack(std::string const & target) 
{
	std::cout << "SC4G-TP " << this->getName() << " use " << target << " at range, and " << this->_ranged_attack_damage << " points of damage!" << std::endl;
}

void	ScavTrap::meleeAttack(std::string const & target) 
{
	std::cout << "SC4G-TP " << this->getName() << " use " << target << " at melee, and " << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void    ScavTrap::takeDamage(unsigned int amount) 
{
	int		hp;

	if (this->_hit_points == 0)
	{
		std::cout << "SC4G-TP " << this->getName() << " hasn't hit points" << std::endl;
	}
	else if ((int)amount <= this->_armor_damage_reduction) 
	{
		std::cout << "SC4G-TP " << this->getName() << " didn't feel anything!" << std::endl;
	}
	else 
	{
		if ((hp = this->_hit_points + this->_armor_damage_reduction - amount) < 0)
			hp = 0;
		std::cout << "SC4G-TP " << this->getName() << " take " << amount << " damage, and has " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}

void	ScavTrap::beRepaired(unsigned int amount) 
{
	int		hp;

	if (this->_hit_points == this->_max_hit_points)
	{
		std::cout << "SC4G-TP "<< this->getName() << " has full health!" <<  std::endl;
	} 
	else 
	{
		if ((hp = this->_hit_points + amount) > 100)
			hp = 100;
		std::cout << "SC4G-TP " << this->getName() << " is repaired by " << amount << " points, and has " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}

void	ScavTrap::challengeNewcomer() 
{
	std::string		Challenge[] = {"1 challenge", "2 challenge", "3 challenge", "4 challenge", "5 challenge"};

	if (this->_energy_points > 0) 
	{
		this->_energy_points -= 25;
		std::cout << "SC4V-TP " << this->_name << " tries to " << Challenge[(rand() % 5)] << " , he has " << this->_energy_points << " energy points." << std::endl;
	} 
	else 
		std::cout << "SC4V-TP " << this->_name << " didn't panic." << std::endl;
}
