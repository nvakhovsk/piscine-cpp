/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 08:48:34 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/02 13:31:12 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include <iostream>
#include <cstdlib>

FragTrap::FragTrap(void): _hit_points(100), _max_hit_points(100), _energy_points(100), _max_energy_points(100), _level(1), _melee_attack_damage(30), _ranged_attack_damage(20), _armor_damage_reduction(5) 
{
	std::cout << "Hey everybody! Check out my package!" << std::endl;
	setName("Default");
}

FragTrap::FragTrap(std::string const name): _hit_points(100), _max_hit_points(100), _energy_points(100), _max_energy_points(100), _level(1), _melee_attack_damage(30), _ranged_attack_damage(20), _armor_damage_reduction(5) 
{
	std::cout << "Glitching weirdness is a term of endearment, right?" << std::endl;
	setName(name);
}

FragTrap::FragTrap(FragTrap const & c) 
{
	std::cout << "Sweet life juice!" << std::endl;
	*this = c;
}

FragTrap::~FragTrap() 
{
	std::cout << " Are you god? Am I dead?" << std::endl;
}

void	FragTrap::setName(std::string const name) 
{
	this->_name = name;
}

std::string		FragTrap::getName(void) const 
{
	return (this->_name);
}

FragTrap &  FragTrap::operator=(FragTrap const & c) 
{
	std::cout << "Hnngh! Empty!" << std::endl;
	this->_name = c.getName();
	this->_hit_points = c._hit_points;
	this->_max_hit_points = c._max_hit_points;
	this->_energy_points = c._energy_points;
	this->_max_energy_points = c._max_energy_points;
	this->_level = c._level;
	this->_melee_attack_damage = c._melee_attack_damage;
	this->_ranged_attack_damage = c._ranged_attack_damage;
	this->_armor_damage_reduction = c._armor_damage_reduction;
	return *this;
}

void	FragTrap::rangedAttack(std::string const & target) 
{
	std::cout << "FR4G-TP " << this->getName() << " use " << target << " at range, and " << this->_ranged_attack_damage << " points of damage!" << std::endl;
}

void	FragTrap::meleeAttack(std::string const & target) 
{
	std::cout << "FR4G-TP " << this->getName() << " use " << target << " at melee, and " << this->_melee_attack_damage << " points of damage!" << std::endl;
}

void    FragTrap::takeDamage(unsigned int amount) 
{
	int		hp;

	if (this->_hit_points == 0)
	{
		std::cout << "FR4G-TP " << this->getName() << " hasn't hit points" << std::endl;
	}
	else if ((int)amount <= this->_armor_damage_reduction) 
	{
		std::cout << "FR4G-TP " << this->getName() << " didn't feel anything!" << std::endl;
	}
	else 
	{
		if ((hp = this->_hit_points + this->_armor_damage_reduction - amount) < 0)
			hp = 0;
		std::cout << "FR4G-TP " << this->getName() << " take " << amount << " damage, and has now " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}

void	FragTrap::beRepaired(unsigned int amount) 
{
	int		hp;

	if (this->_hit_points == this->_max_hit_points)
	{
		std::cout << "FR4G-TP "<< this->getName() << " has full health!" <<  std::endl;
	} 
	else 
	{
		if ((hp = this->_hit_points + amount) > 100)
			hp = 100;
		std::cout << "FR4G-TP " << this->getName() << " is repaired by " << amount << " points, and has now " << hp << " hit points." << std::endl;
		this->_hit_points = hp;
	}
}

void    FragTrap::vaulthunter_dot_exe(std::string const & target) 
{

	std::string		Attacks[] = {"1 attack", "2 attack", "3 attack", "4 attack", "5 attack"};

	if (this->_energy_points > 0) 
	{
		this->_energy_points -= 15;
		std::cout << "FR4G-TP " << this->_name << " uses " << Attacks[(rand() % 5)] << " on " << target << " and has now " << this->_energy_points << " energy points left!" << std::endl;
	} 
	else 
		std::cout << "FR4G-TP " << this->_name << " is exhausted!" << std::endl;
}
